import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }
    @Test
    public void testMultiply(){
	double expectedResult = 6.5;
	assertEquals(expectedResult, Calculator.multiply(3.25, 2), 1e-6);
    }
    @Test
    public void testDivide(){
	double expectedResult = 2;
	assertEquals(expectedResult, Calculator.divide(4, 2), 1e-6);
    }
    @Test
    public void testAbs(){
	double expectedResult = 0.5;
	assertEquals(expectedResult, Calculator.abs(-0.5), 1e-6);
    }
    @Test
    public void testPower(){
	double expectedResult = 25;
	assertEquals(expectedResult, Calculator.power(5, 2), 1e-6);
    }
}
